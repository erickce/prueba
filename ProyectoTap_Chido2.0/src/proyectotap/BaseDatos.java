package proyectotap;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class BaseDatos {

    private int cont;

    public BaseDatos() {
        //conectar();
        //cont = getid() + 1;
    }

    /*public int getid() {
        int x = 0;
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "SELECT MAX(ID) from Jugadores;";
            ResultSet rs = stmt.executeQuery(sql);
            x = rs.getInt(sql);

            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return x;
    }*/
    public void conectar() {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public int iniciar(int id, String nick, String contra) {
        int b = 0;

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "Select ID FROM Jugadores WHERE NICK = '" + nick + "';";
            stmt.executeQuery(sql);
            ResultSet rs = stmt.executeQuery(sql);
            b = rs.getInt("ID");
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return b;
    }

    public int registrar(int id, String nick, String contra) {
        int b = 0;

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "INSERT INTO Jugadores (ID,NICK,CONTRA,PUNTAJETOT,RECORD) "
                    + "VALUES (" + id + ",'" + nick + "','" + contra + "',0,0);";
            stmt.executeUpdate(sql);
            b = id;
            cont = id;
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return b;
    }
    
    public String nickGet(){
        String s = "";
        
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "Select NICK FROM Jugadores WHERE ID = " + cont + ";";
            stmt.executeQuery(sql);
            ResultSet rs = stmt.executeQuery(sql);
            s = sql;
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        
        return s;
    }

    public void mostrarUsuarios() {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players", "postgres", "abimael250399");
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Jugadores;");
            while (rs.next()) {
                int id = rs.getInt("ID");
                String nombre = rs.getString("NICK");
                String contra = rs.getString("CONTRA");
                int numero = rs.getInt("PUNTAJETOT");
                int record = rs.getInt("RECORD");
                System.out.println("ID = " + id);
                System.out.println("nombre = " + nombre);
                System.out.println("contra = " + contra);
                System.out.println("puntaje = " + numero);
                System.out.println("record = " + record);
                System.out.println();
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("-----------------------------------------------------------");

    }
}
