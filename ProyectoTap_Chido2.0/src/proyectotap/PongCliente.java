package proyectotap;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.lang.Thread.sleep;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;

public class PongCliente extends javax.swing.JInternalFrame implements Runnable, KeyListener, Observer {

    private int cont, cont2;
    private BaseDatos bd = new BaseDatos();

    public PongCliente() {
        initComponents();
        //Teclado
        setFocusable(closable);
        addKeyListener(this);
        //No agranda la pantalla
        setResizable(false);
        //Socket
        Servidor s = new Servidor(6000);
        s.addObserver(this);
        //Holo socket
        Thread t = new Thread(s);
        t.start();
        //Hilo
        Test();
    }

    public void Test() {
        Thread roca1 = new Thread(this);
        roca1.setName("Pelota");
        roca1.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        Jugador1 = new javax.swing.JLabel();
        Jugador2 = new javax.swing.JLabel();
        Pelota = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jMarca1 = new javax.swing.JLabel();
        jMarca2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Cliente");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 10, 180, 230));
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 250, 180, -1));

        jButton1.setText("Enviar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 280, -1, -1));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        Jugador1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectotap/Espada verde.png"))); // NOI18N

        Jugador2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectotap/Espada Roja.png"))); // NOI18N

        Pelota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectotap/estrella-de-la-muerte.png"))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        jLabel1.setText("Marcdor");

        jMarca1.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        jMarca1.setText("0");

        jMarca2.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        jMarca2.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Jugador2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 276, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(95, 95, 95)
                .addComponent(Pelota)
                .addGap(148, 148, 148)
                .addComponent(Jugador1)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(149, 149, 149)
                .addComponent(jMarca1)
                .addGap(122, 122, 122)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jMarca2)
                .addGap(130, 130, 130))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(Jugador2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jMarca2)
                                        .addGap(100, 100, 100)
                                        .addComponent(Pelota))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addComponent(Jugador1)))
                                .addGap(0, 128, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jMarca1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 590, 300));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectotap/Estrellas.gif"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 820, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String mensaje = "2: " + jTextField1.getText() + "\n";

        jTextArea1.append(mensaje);

        Cliente c = new Cliente(5000, mensaje);
        Thread t = new Thread(c);
        t.start();

        jTextField1.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Jugador1;
    private javax.swing.JLabel Jugador2;
    private javax.swing.JLabel Pelota;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jMarca1;
    private javax.swing.JLabel jMarca2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables

   public void run() {

        int y1 = (int) (20 * (Math.random())),
                x1 = (int) (20 * (Math.random()));
        double h1 = (Math.random() * 2), h2 = (Math.random() * 2);
        while (x1 < 10) {
            x1 = (int) (20 * (Math.random()));
        }
        if (y1 == 0) {
            y1 = 5;
        }
        if (h1 > 1) {
            y1 *= (-1);
        }
        if (h2 > 1) {
            x1 *= (-1);
        }
        while (true) {
            try {
                Thread.sleep(120);
                if (Pelota.getY() < (Jugador1.getY() + 70) && Pelota.getY() > (Jugador1.getY() - 10) && (Pelota.getLocation().x + x1) < 15) {
                    x1 *= (-1);
                } else if (Pelota.getY() < (Jugador2.getY() + 70) && Pelota.getY() > (Jugador2.getY() - 10) && (Pelota.getLocation().x + x1) > jPanel1.getWidth() - 40) {
                    x1 *= (-1);
                }
                if ((Pelota.getLocation().x + x1) < -15) {
                    cont2++;
                    jMarca2.setText(String.valueOf(cont2));
                    break;
                    // x1 *= (-1);
                } else if ((Pelota.getLocation().x + x1) > jPanel1.getWidth() - 15) {
                    cont++;
                    jMarca1.setText(String.valueOf(cont));
                    break;
                }
                if ((Pelota.getLocation().y + y1) < 0 || (Pelota.getLocation().y + y1) > jPanel1.getHeight() - 25) {
                    y1 *= (-1);
                }
                Pelota.setLocation(Pelota.getLocation().x + x1, Pelota.getLocation().y + y1);
            } catch (InterruptedException e) {
            }
        }
        System.out.println(cont + " / " + cont2);
        if (cont == 7 || cont2 == 7) {
            JOptionPane.showMessageDialog(null, "Termino el juego");
            Ganador(cont, cont2);
        } else {
            Pelota.setLocation(295, 150);
            run();
        }

    }

    public void Ganador(int cont1, int cont2) {
        if (cont1 == 7) {
            String s = bd.nickGet();
            JOptionPane.showMessageDialog(null, s + " gana");
        } else {
            JOptionPane.showMessageDialog(null, "Jugador 2 gana");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == 'W' || e.getKeyChar() == 'w') {
            if (Jugador2.getY() > 0) {
                Jugador2.setLocation(Jugador2.getX(), Jugador2.getY() - 5);
            }

        }
        if (e.getKeyChar() == 'S' || e.getKeyChar() == 's') {
            if (Jugador2.getY() < 200) {
                Jugador2.setLocation(Jugador2.getX(), Jugador2.getY() + 5);
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getExtendedKeyCode() == KeyEvent.VK_UP) {
            if (Jugador1.getY() > 0) {
                Jugador1.setLocation(Jugador1.getX(), Jugador1.getY() - 5);
            }
        }

        if (e.getExtendedKeyCode() == KeyEvent.VK_DOWN) {
            if (Jugador1.getY() < 200) {
                Jugador1.setLocation(Jugador1.getX(), Jugador1.getY() + 5);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void update(Observable o, Object arg) {
        jTextArea1.append((String) arg);
    }
}
